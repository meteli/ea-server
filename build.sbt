lazy val root = (project in file(".")).
  settings(net.virtualvoid.sbt.graph.Plugin.graphSettings:_*).
  settings(Revolver.settings: _*).
  settings(
    name :="ea-server",
    version := "0.1",
    scalaVersion := "2.11.7",
    assemblyOutputPath in assembly := {
      file(s"out/${name.value.toLowerCase}.jar")
    },
    javaOptions in Revolver.reStart += "-Xmx2g",
    //, Revolver.enableDebugging(port = 5050, suspend = true)
    resolvers ++= Seq(
      "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
      Opts.resolver.sonatypeSnapshots,
      "spray repo" at "http://repo.spray.io",
      "spray nightlies repo" at "http://nightlies.spray.io"
    ),
    libraryDependencies ++= Seq(
      "org.scala-lang"             %  "scala-reflect"     % "2.11.7",
      "org.scala-lang.modules"     %% "scala-parser-combinators"         % "1.0.4",
      "org.scala-lang.modules"     %% "scala-xml"         % "1.0.4",
      "com.typesafe.akka"          %% "akka-actor"        % "2.3.14",
      "com.typesafe.akka"          %% "akka-slf4j"        % "2.3.14",
//      "com.typesafe.akka"          %% "akka-remote"       % "2.3.12",
//      "com.typesafe.akka"          %% "akka-agent"        % "2.3.12",
      "com.typesafe.scala-logging" %% "scala-logging"     % "3.1.0",
//      "io.spray"                   %% "spray-can"         % "1.3.3",
//      "io.spray"                   %% "spray-routing"     % "1.3.3",
//      "io.spray"                   %% "spray-json"        % "1.3.2",
      "joda-time"                  %  "joda-time"         % "2.8.1",
      "org.scalikejdbc"            %% "scalikejdbc-async" % "0.5.+",
      "com.github.mauricio"        %% "postgresql-async"  % "0.2.+",
      "org.slf4j"                  %  "slf4j-simple"      % "1.7.+",
      "org.scalatest"              %  "scalatest_2.11"    % "2.2.4"    % "test"
    ),
    fork := true
  )
