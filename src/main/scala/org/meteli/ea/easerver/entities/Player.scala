package org.meteli.ea.easerver.entities

import org.meteli.ea.easerver.actors.PlayerActor
import org.meteli.ea.easerver.common.{ActorSystems, Coord}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

case class Player (id: Long, name: String, patch: Coord = Coord(), coordInPatch: Coord = Coord()) extends Entity {
  val actor = ActorSystems.main.actorOf(PlayerActor.props(this), s"Player$id")

  def setTarget(patch: Coord, coordInPatch: Coord) = {

  }

  def setTarget(entity: Entity) = {

  }

  def setTarget(id: Long) = {
    println(s"Setting target by id = $id")
  }

}
