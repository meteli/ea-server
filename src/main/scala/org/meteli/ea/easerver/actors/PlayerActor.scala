package org.meteli.ea.easerver.actors

import akka.actor.{Props, Actor}
import org.meteli.ea.easerver.common.Coord
import org.meteli.ea.easerver.entities.{Entity, Player}

object PlayerActor {
  def props(player: Player): Props = Props(new PlayerActor(player))

  case class SetTargetByCoordinates(patch: Coord, coordInPatch: Coord)
  case class SetTargetByEntity(entity: Entity)
  case class SetTargetById(id: Long)
}

class PlayerActor(player: Player) extends Actor {
  def receive = {
    case PlayerActor.SetTargetByCoordinates(patch, coordInPatch) => player.setTarget(patch, coordInPatch)
    case PlayerActor.SetTargetByEntity(entity) => player.setTarget(entity)
    case PlayerActor.SetTargetById(id) => player.setTarget(id)

    case _ =>
  }
}
