package org.meteli.ea.easerver.common

import akka.actor.ActorSystem

object ActorSystems {
  lazy val main = ActorSystem("ea-server")
}
