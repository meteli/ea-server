package org.meteli.ea.easerver.common

case class Coord(x: Long, y: Long, z: Long = 0)

object Coord {
  def apply(): Coord = new Coord(0, 0)
}