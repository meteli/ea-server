package org.meteli.ea.easerver

import org.meteli.ea.easerver.actors.PlayerActor
import org.meteli.ea.easerver.entities.Player

object Application extends App {
  val p = new Player(42, "player42")

  p.actor ! PlayerActor.SetTargetById(1)
}
